import got from "./data.js"

const data = got.houses

const housesNames = data.map(eachHouseName=>eachHouseName.name)

const peoplesData = data.reduce((acc,house)=>{
     
        acc[house.name] = house.people.reduce((acc,eachHouse)=>{
                const eachPerson = {
                    name:eachHouse.name,
                    description:eachHouse.description,
                    image:eachHouse.image,
                    wikiLink:eachHouse.wikiLink
                }
                acc = [...acc,eachPerson]
                return  acc
        },[])
        return acc
},[])






const familyList = document.getElementById("family-list")


function createAndUpdateFamilyItem(family){

  const familyButton = document.createElement("button")
  familyButton.textContent = family
  familyButton.classList.add("family-button")
  familyButton.setAttribute("id",family)

  familyList.appendChild(familyButton)

  familyButton.onclick = ()=>{
    
    const familyListItems = document.getElementsByTagName("li")
    for(let familyItem of familyListItems){
      if(familyItem.id === family){
        familyItem.style.display = "block"
      }else{
        familyItem.style.display = "none"
      }

    }
  }
}



for(let house of housesNames){
  createAndUpdateFamilyItem(house)
}



const peopleListContainer = document.getElementById("userList")

function createAndUpdatePeople(people,house){

    for(let person of people){
        const personListElement = document.createElement("li")
        personListElement.classList.add("person-item")
        personListElement.setAttribute("id",house)

        const profileImage = document.createElement("img")
        profileImage.classList.add("profilePic")
        profileImage.setAttribute("src",person.image)


        const personNameHeader = document.createElement("h2")
        personNameHeader.classList.add("person-name-header")
        personNameHeader.textContent = person.name
        

        const descriptionPara = document.createElement("p")
        descriptionPara.textContent = person.description

        const knowMoreButton = document.createElement("button")
        knowMoreButton.classList.add("know-more-button")
   
        knowMoreButton.innerText = "Know More"
        personListElement.appendChild(profileImage)
        personListElement.appendChild(personNameHeader)
        personListElement.appendChild(descriptionPara)
        personListElement.appendChild(knowMoreButton)

        peopleListContainer.appendChild(personListElement)
    }
}


for(let house in peoplesData){
  createAndUpdatePeople(peoplesData[house],house)
}


const searchInput = document.getElementById("searchInput")

searchInput.addEventListener("keyup",event=>{
  const familyListItems = document.getElementsByTagName("li")

  for(let family of familyListItems){
    const name = family.getElementsByTagName("h2")
    const nameInList = (Array.from(name)[0].textContent).toLocaleLowerCase()
    let userInput = event.target.value;
    userInput = userInput.toLocaleLowerCase()
    if(nameInList.includes(userInput)){
      family.style.display = "block"
    }else if(userInput.length === 0){
        family.style.display = "block"
    }else{
      family.style.display = "none"
    }

  }

})